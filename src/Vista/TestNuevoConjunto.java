/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import Negocio.Colegio;
import Negocio.ColegioConjunto;
import Util.Conjunto;
import java.awt.BorderLayout;
import java.util.Scanner;

/**
 *
 * @author MADARME
 */
public class TestNuevoConjunto {

    public static void main(String[] args) {
        try {
            Conjunto<Integer> c1 = new Conjunto(9);
            Conjunto<Integer> c2 = new Conjunto(20);
            Conjunto<Integer> c3 = new Conjunto(30);
            Scanner s = new Scanner(System.in);
            System.out.println("Escriba una de las funciones del conjunto\n1: Test de Concatenacion\n2: Test de ordenamientos\n"
                    + "3: Test de eliminar");
            int numero = s.nextInt();
            llenarConjuntos(c1, c2, c3);
            switch (numero) {
                case 1:
                testConcantenarConjuntos(c1, c2, c3);
                break;
                case 2:
                    System.out.println("¿Escoja un conjunto entre 1,2,3 para trabajar: ");
                    int conjunto = s.nextInt();
                    switch(conjunto){
                        case 1: testDeOrdenamiento(c1);
                        break;
                        case 2: testDeOrdenamiento(c2);
                        break;
                        case 3: testDeOrdenamiento(c3);
                        break;
                        default: System.out.println("No coloco un conjunto valido");
                    }           
                break;
                case 3:
                    System.out.println(c2.toString());
                    System.out.print("Escriba un la pocision (entre 0 y 19) que desee eliminar: ");
                    int pocision = s.nextInt();
                    testRemover(c2, pocision);
                    System.out.println("Compruebe si se ha elimnador su pocision: \n"+c2.toString());    
                    System.out.println("El nuevo tamaño del conjunto es: "+c2.getCapacidad());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //probar todos los métodos públicos(imprimir)
        //Recomendación: Realicen método para crear cada una de las pruebas.
    }

    public static void testConcantenarConjuntos(Conjunto c1, Conjunto c2, Conjunto c3) throws Exception {
        System.out.println("Este es el conjunto C1\n"+c1.toString());
        System.out.println("Este es el conjunto C2\n"+c2.toString());
        System.out.println("Este es el conjunto c3\n"+c3.toString());
        c1.concatenar(c2);
        System.out.println("Conjuntos C1 y C2 concatenados\n" + c1.toString());
        c1.concatenarRestrictivo(c3);
        System.out.println("Conjuntos anteriormente concatenados más C3 concatenado Restrictivamente\n" + c1.toString());
    }

    public static void testDeOrdenamiento(Conjunto c1) throws Exception {
        System.out.println(c1.toString());
        Conjunto conjunto2 = c1;
        c1.ordenar();
        System.out.println("Ordenamiento por insercion\n" + c1.toString());
        conjunto2.ordenarBurbuja();
        System.out.println("Ordenamiento por Burbuja\n" + conjunto2.toString());
    }
    
    public static void testRemover(Conjunto c1,int pocision) throws Exception{
        if( pocision>=0 && pocision < c1.getLength()){
            System.out.println("\nSe va a eliminar el objeto: "+c1.get(pocision));
            c1.remover(c1.get(pocision));
        }else throw new Exception("Pocisión erronea");
    }

    public static void llenarConjuntos(Conjunto c1, Conjunto c2, Conjunto c3) throws Exception {
        c1.adicionarElemento(30);    c2.adicionarElemento(12);  c3.adicionarElemento(98);
        c1.adicionarElemento(3);     c2.adicionarElemento(11);  c3.adicionarElemento(9);
        c1.adicionarElemento(0);     c2.adicionarElemento(31);  c3.adicionarElemento(8);
        c1.adicionarElemento(21);    c2.adicionarElemento(89);  c3.adicionarElemento(32);
        c1.adicionarElemento(10);    c2.adicionarElemento(98);  c3.adicionarElemento(33); 
        c1.adicionarElemento(37);    c2.adicionarElemento(1);   c3.adicionarElemento(31);
        c1.adicionarElemento(40);    c2.adicionarElemento(32);  c3.adicionarElemento(73);
        c1.adicionarElemento(20);    c2.adicionarElemento(2);   c3.adicionarElemento(92);
        c1.adicionarElemento(32);    c2.adicionarElemento(40);  c3.adicionarElemento(78);
                                     c2.adicionarElemento(10);  c3.adicionarElemento(28);
                                     c2.adicionarElemento(16);  c3.adicionarElemento(18);
                                     c2.adicionarElemento(19);  c3.adicionarElemento(38);
                                     c2.adicionarElemento(41);  c3.adicionarElemento(68);
                                     c2.adicionarElemento(54);  c3.adicionarElemento(48);
                                     c2.adicionarElemento(9);   c3.adicionarElemento(43);
                                     c2.adicionarElemento(92);  c3.adicionarElemento(65);
                                     c2.adicionarElemento(42);  c3.adicionarElemento(67);
                                     c2.adicionarElemento(87);  c3.adicionarElemento(52);
                                     c2.adicionarElemento(68);  c3.adicionarElemento(37);
                                     c2.adicionarElemento(99);  c3.adicionarElemento(57);
                                                                c3.adicionarElemento(77);
                                                                c3.adicionarElemento(17);
                                                                c3.adicionarElemento(56);
                                                                c3.adicionarElemento(91);
                                                                c3.adicionarElemento(54);
                                                                c3.adicionarElemento(10);
                                                                c3.adicionarElemento(75);
                                                                c3.adicionarElemento(39); 
                                                                c3.adicionarElemento(5); 
                                                                c3.adicionarElemento(89); 
                                                              
    }   
}
